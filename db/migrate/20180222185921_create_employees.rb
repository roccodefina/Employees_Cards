class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :last_name
      t.string :dni
      t.string :cellphone
      t.string :email
      t.string :bank
      t.string :account_type
      t.string :account_number
      t.string :phone
      t.string :emergency_phone
      t.references :department, foreign_key: true

      t.timestamps
    end
  end
end
