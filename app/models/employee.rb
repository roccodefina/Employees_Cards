class Employee < ApplicationRecord
  belongs_to :department

  def self.search(search)
    if search
      where('first_name LIKE ?', "%#{search}%")
    else
      search = []
    end
  end

end
