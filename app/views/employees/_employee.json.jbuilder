json.extract! employee, :id, :first_name, :last_name, :dni, :cellphone, :email, :bank, :account_type, :account_number, :phone, :emergency_phone, :department_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
