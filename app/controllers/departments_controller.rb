class DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_company!

  # GET /departments
  # GET /departments.json
  def index
    @departments = Department.all
    @department = Department.new
    @employees = Employee.all
    if params[:search]
      @search = Employee.all.where('first_name LIKE ?', "%#{params[:search]}%")
    else
      params[:search] = []
    end
    # @value = nil
  end

  # GET /departments/1
  # GET /departments/1.json
  def show
  end

  # GET /departments/new
  def new
    @department = Department.new
  end

  # GET /departments/1/edit
  def edit
    @departments = Department.all
    @employees = Employee.all
    if params[:search]
      @search = Employee.all.where('first_name LIKE ?', "%#{params[:search]}%")
    else
      params[:search] = []
    end
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = Department.new(department_params.merge!({company_id:current_company.id}))

    respond_to do |format|
      if @department.save
        format.html { redirect_to root_path, notice: 'El departamento fue creado exitosamente.' }
        format.json { render :index, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to root_path, notice: 'El departamento fue editado exitosamente.' }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to departments_url, notice: 'El departamento fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:name)
    end
end
